package com.tsconsulting.dsubbotin.tm.api;

import com.tsconsulting.dsubbotin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
