package com.tsconsulting.dsubbotin.tm.api;

public interface ICommandController {

    void displayWelcome();

    void displayAbout();

    void displayVersion();

    void displayInfo();

    void displayHelp();

    void displayCommands();

    void displayArguments();

    void displayArgError();

    void displayCommandError();

    void exit();

}
