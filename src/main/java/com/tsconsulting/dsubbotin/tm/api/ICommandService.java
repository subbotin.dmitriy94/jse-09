package com.tsconsulting.dsubbotin.tm.api;

import com.tsconsulting.dsubbotin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
