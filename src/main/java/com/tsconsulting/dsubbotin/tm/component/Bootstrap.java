package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.ICommandController;
import com.tsconsulting.dsubbotin.tm.api.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.ICommandService;
import com.tsconsulting.dsubbotin.tm.constant.ArgumentConst;
import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.controller.CommandController;
import com.tsconsulting.dsubbotin.tm.repository.CommandRepository;
import com.tsconsulting.dsubbotin.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        parseArgs(args);
        process();
    }

    private void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exit();
    }

    private void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            default:
                commandController.displayArgError();
                break;
        }
    }

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                commandController.displayCommandError();
                break;
        }
    }

}
